---
stage: Systems
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# GitLab Rails Console Cheat Sheet **(FREE SELF)**

This is the GitLab Support Team's collection of information regarding the GitLab Rails
console, for use while troubleshooting. It is listed here for transparency,
and for users with experience with these tools. If you are currently
having an issue with GitLab, it is highly recommended that you first check
our guide on [our Rails console](../operations/rails_console.md),
and your [support options](https://about.gitlab.com/support/), before attempting to use
this information.

WARNING:
Some of these scripts could be damaging if not run correctly,
or under the right conditions. We highly recommend running them under the
guidance of a Support Engineer, or running them in a test environment with a
backup of the instance ready to be restored, just in case.

WARNING:
As GitLab changes, changes to the code are inevitable,
and so some scripts may not work as they once used to. These are not kept
up-to-date as these scripts/commands were added as they were found/needed. As
mentioned above, we recommend running these scripts under the supervision of a
Support Engineer, who can also verify that they continue to work as they
should and, if needed, update the script for the latest version of GitLab.

## Mirrors

### Find mirrors with "bad decrypt" errors

This content has been converted to a Rake task, see [verify database values can be decrypted using the current secrets](../raketasks/check.md#verify-database-values-can-be-decrypted-using-the-current-secrets).

### Transfer mirror users and tokens to a single service account

This content has been moved to [Troubleshooting Repository mirroring](../../user/project/repository/mirror/index.md#transfer-mirror-users-and-tokens-to-a-single-service-account-in-rails-console).

## Merge requests

## CI

This content has been moved to [Troubleshooting CI/CD](../../ci/troubleshooting.md).

## License

This content has been moved to [Activate GitLab EE with a license file or key](../../user/admin_area/license_file.md).

## Registry

### Registry Disk Space Usage by Project

Find this content in the [Container Registry troubleshooting documentation](../packages/container_registry.md#registry-disk-space-usage-by-project).

### Run the Cleanup policy now

Find this content in the [Container Registry troubleshooting documentation](../packages/container_registry.md#run-the-cleanup-policy-now).

## Sidekiq

This content has been moved to [Troubleshooting Sidekiq](../sidekiq/sidekiq_troubleshooting.md).

## Geo

### Artifacts

Moved to [Geo replication troubleshooting](../geo/replication/troubleshooting.md#find-failed-artifacts).

### Repository verification failures

Moved to [Geo replication troubleshooting](../geo/replication/troubleshooting.md#repository-verification-failures).

### Resync repositories

Moved to [Geo replication troubleshooting](../geo/replication/troubleshooting.md#resync-repositories).

### Blob types

Moved to [Geo replication troubleshooting](../geo/replication/troubleshooting.md#blob-types).

## Generate Service Ping

This content has been moved to [Service Ping Troubleshooting](../../development/service_ping/troubleshooting.md).
